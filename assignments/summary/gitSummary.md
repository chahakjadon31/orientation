**GIT BASICS**

**Version Control System(VCS)** for tracking changes in computer files
  * Coordinates work between multiple developers
  * What changes are made and by whom
  * Revert back any time
  * Local and remote repos
  * Distributed Version control

**CONCEPTS OF GIT**
 	[Concepts of git](https://docs.google.com/document/d/10pkYQk7AOwZCDvjOKEur_w5xf1Xo2-JwqGIxmlzfnBA/edit)

   **GIT INTERNALS** 
  [Git Internals](https://docs.google.com/document/d/1uMabd3cFbQ5qH9i0pDRfT6u3N-tGXFgJPQDjvZ91WXI/edit)

 **BASIC COMMANDS**
  * $ git init: Initialize the local repository
  * $ git add file: Add File(s) to Staging
    * workspace -> staging
  * $ git status: Check status of working tree
  * $ git commit: Commit changes to the Staging and moves the files to local repository
  (_git commit -m 'changed file'_ is used)
    * staging -> local repo
    * workspace -> local repo (_git commit -a_)
  * $ git push: Push to remote repository 
    * local repo -> remote repo
  * $ git pull: Pull Latest From Remote repository
    * remote repo -> workspace
  * $ git clone: Clone repository into a new Directory
    * remote repo -> workspace
  * $ git branch branch_name: to create a branch
  * $ git checkout branch_name: to chnage the branch you're working on
  * $ git merge branch_name: to merge two branches

  





  





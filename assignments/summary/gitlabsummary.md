**GIT LAB BASICS**

Services that allow to _host your project_ on your _remote repository_ & have some additional features to help in software development lifecycle (SDLC)

**Features**
* Managing
* Sharing
* Wiki
* Bug Tracking
* CI & CD (Continuous Integration & continuous Deployment)

**ISSUES**: If you are working, you would want a single place to combine all your information at, this is where issues become useful.  

**Forking**:
A fork is a copy of project
Forking a repo/project allows you to make changes without affecting the original project

**Merge**:
In order to merge branches, you can use the git command: _git merge branch_name_

**Important COMMANDS**
 * $ git clone: Clone repository into a new Directory
    * remote repo -> workspace
  * $ git branch branch_name: to create a branch
  * $ git checkout branch_name: to chnage the branch you're working on
  * $ git merge branch_name: to merge two branches




